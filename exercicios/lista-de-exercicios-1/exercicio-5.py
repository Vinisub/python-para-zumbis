# -*- coding: utf-8 -*-
preco = float(input('Preço do Produto: '))
porcentagem = float(input('Percentual de Desconto: '))

desconto = preco * (porcentagem / 100)
valorTotal = preco - desconto

print('valor do desconto R$: %.2f' %desconto)
print('Valor com desconto R$: %.2f' %valorTotal)
